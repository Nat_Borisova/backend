﻿using NHibernate;

namespace Product
{
    public class DatabaseStorage : IStorage<NHProducts>
    {
        private readonly ISession _session;

        public DatabaseStorage(ISession session)
        {
            _session = session;
        }

        public void Add(NHProducts obj)
        {
            using (var tx = _session.BeginTransaction())
            {
                _session.Save(obj);
                tx.Commit();
            }
        }

        public void Delete(NHProducts obj)
        {
            using (var tx = _session.BeginTransaction())
            {
                _session.Delete(obj);
                tx.Commit();
            }
        }

        NHProducts IStorage<NHProducts>.Get(int index)
        {
            using (var tx = _session.BeginTransaction())
            {
                var product = _session.Get<NHProducts>(index);
                tx.Commit();
                return product;
            }
        }

        public void Update(NHProducts obj)
        {
            using (var tx = _session.BeginTransaction())
            {
                _session.Update(obj);
                tx.Commit();
            }
        }
    }
}
