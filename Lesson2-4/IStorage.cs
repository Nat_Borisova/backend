namespace Product
{
    public interface IStorage<T>
    {
        void Add(T obj);
        void Delete(T obj);
        T Get(int index);
        void Update(T obj);
    }
}