namespace Product
{
    public class Product<TId>
    {
        public TId Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id:{Id}; Name:{Name}";
        }

        public NHProducts ToNhProducts(Product<int> obj)
        {
            return new NHProducts {Id = obj.Id, Name = obj.Name};
        }
    }
}