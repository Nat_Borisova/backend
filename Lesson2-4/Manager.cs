using System;

namespace Product
{
    public class Manager
    {
        private readonly IStorage<NHProducts> _storage;
        private readonly ILogger _logger;

        public Manager(IStorage<NHProducts> storage, ILogger logger)
        {
            _storage = storage;
            _logger = logger;
        }

        public void Add(Product<int> obj)
        {
            _storage.Add(obj.ToNhProducts(obj));
            _logger.Log($"Добавлен новый элемент: {obj.ToString()}");
        }

        public void Delete(Product<int> obj)
        {
            _storage.Delete(obj.ToNhProducts(obj));
            _logger.Log($"Удален элемент: {obj.ToString()}");
        }

        public Product<int> Get(int index)
        {
            var obj = _storage.Get(index);
            _logger.Log($"Получен элемент: {obj.ToString()}");

            return new Product<int>() {Id = obj.Id, Name = obj.Name};
        }

        public void Update(Product<int> obj)
        {
            _storage.Update(obj.ToNhProducts(obj));
            _logger.Log($"Обновлен элемент: {obj.ToString()}");
        }
    }
}