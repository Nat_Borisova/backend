﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Product
{
    public class NHProducts
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

    }

    public class NHProductsMap : ClassMapping<NHProducts>
    {
        public NHProductsMap()
        {
            Table("products");

            Id(x => x.Id);
            Property(x => x.Name, map => map.NotNullable(false));
        }
    }

    public class NHLogs
    {
        public virtual int Id { get; set; }
        public virtual string Time { get; set; }
        public virtual string Text { get; set; }
    }

    public class NHLogsMap : ClassMapping<NHLogs>
    {
        public NHLogsMap()
        {
            Table("logs");

            Id(x => x.Id, m => m.Generator(Generators.Native));

            Property(x => x.Time, map => map.NotNullable(false));
            Property(x => x.Text, map => map.NotNullable(false));

        }
    }
}
