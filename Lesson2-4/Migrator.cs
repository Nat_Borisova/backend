﻿using FluentMigrator;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Product
{
    [Migration(1)]
    public class BaseMigration : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("products")
                .WithColumn("id").AsInt32().NotNullable()
                .WithColumn("name").AsAnsiString().NotNullable();

            Create.Sequence("logs_id_seq");

            Create.Table("logs")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("logs_id_seq"))
                .WithColumn("time").AsAnsiString().NotNullable()
                .WithColumn("text").AsAnsiString().NotNullable();
        }
    }

    public class NextVal
    {
        private readonly string _name;
        public NextVal(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return $"nextval('{_name}')";
        }
    }
    public class MigrationsRunner
    {
        private readonly string _conn;

        public MigrationsRunner(string conn)
        {
            _conn = conn;
        }

        public void Run()
        {
            var serviceProvider = CreateServices();
            using (var scope = serviceProvider.CreateScope())
            {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                // Execute the migrations
                runner.MigrateUp();
            }
        }

        private IServiceProvider CreateServices()
        {
            return new ServiceCollection()
                // Add common FluentMigrator services
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    // Add support to FluentMigrator
                    .AddPostgres()
                    // Set the connection string
                    .WithGlobalConnectionString(_conn)
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(BaseMigration).Assembly).For.Migrations())
                // Enable logging to console in the FluentMigrator way
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                // Build the service provider
                .BuildServiceProvider(false);
        }
    }
}
