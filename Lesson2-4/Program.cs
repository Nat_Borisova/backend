﻿using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using System.Reflection;
using System;

namespace Product
{
    internal static class Program
    {
        private static void Main(string[] args)
        {

            const string connString = "server=127.0.0.1;port=5432;database=product_db;user id=username;password=123;";

            var runner = new MigrationsRunner(connString);
            runner.Run();

            var nConf = GetNHConfig(connString);
            using (var factory = nConf.BuildSessionFactory())
            {
                using (var session = factory.OpenSession())
                {
                    var obj1 = new Product<int> {Id = 1, Name = "Fruit"};
                    var obj2 = new Product<int> {Id = 2, Name = "Coffee"};
                    var obj3 = new Product<int> {Id = 3, Name = "Lemonade"};

                    var storage = new DatabaseStorage(session);
                    var logger = new DatabaseLogger(session);
            
                    var manager = new Manager(storage, logger);

//                    manager.Add(obj1);
//                    manager.Add(obj2);
//                    manager.Add(obj3);

                    manager.Delete(obj1);

                    obj1.Name = "Tea";
                    manager.Update(obj3);

                    manager.Get(1);
                }
            }

        }

        private static NHibernate.Cfg.Configuration GetNHConfig(string conn)
        {
            var config = new NHibernate.Cfg.Configuration()
                .SetNamingStrategy(ImprovedNamingStrategy.Instance)
                .DataBaseIntegration(db =>
                {
                    db.ConnectionString = conn;
                    db.Dialect<PostgreSQL83Dialect>();
                    db.LogFormattedSql = true;
                    db.LogSqlInConsole = true;
                });
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetAssembly(typeof(NHProducts)).GetExportedTypes());
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            config.AddMapping(mapping);
            config.SessionFactory().GenerateStatistics();
            return config;
        }
    }
}
