namespace Product
{
    public interface ILogger
    {
        void Log(string text);
    }
}