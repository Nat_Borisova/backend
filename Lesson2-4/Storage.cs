using System.Collections.Generic;
using NHibernate;

namespace Product
{
    public class Storage : IStorage<Product<int>>
    {
        public readonly List<Product<int>> ProductList = new List<Product<int>>();

        public void Add(Product<int> obj)
        {
            ProductList.Add(obj);
        }

        public void Delete(Product<int> obj)
        {
            ProductList.Remove(obj);
        }

        public Product<int> Get(int index)
        {
            return index > ProductList.Count ? new Product<int>() : ProductList[index];
        }

        public void Update(Product<int> obj)
        {
            //obj.Name = newValue;
        }
    }
}