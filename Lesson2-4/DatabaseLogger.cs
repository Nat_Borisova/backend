using System;
using NHibernate;

namespace Product
{
    public class DatabaseLogger : ILogger
    {
        private readonly ISession _session;

        public DatabaseLogger(ISession session)
        {
            _session = session;
        }

        public void Log(string text)
        {
            using (var tx = _session.BeginTransaction())
            {
                var obj = new NHLogs {Text = text, Time = Convert.ToString(DateTime.Now)};
                _session.Save(obj);
                tx.Commit();
            }
        }
    }
}