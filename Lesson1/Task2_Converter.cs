﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Task2
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var elem = new Model() {Id = "1", Value = "Ivan"};
            var converter = new Converter();
            Console.WriteLine(converter.Serialize(elem));
            var obj = converter.Deserialize("id:1;Value:Sova");
            Console.WriteLine(converter.Serialize(obj));
        }
    }

    public interface IConverter<T>
        where T : class
    {
        string Serialize(T obj);
        T Deserialize(string str);
    }

    public class Model
    {
        [Caption("Идентификатор")] public string Id { get; set; }
        public string Value { get; set; }
    }

    public class CaptionAttribute : Attribute
    {
        public string Caption { get; set; }

        public CaptionAttribute(string name)
        {
            Caption = name;
        }
    }

    public class Converter : IConverter<Model>
    {
        public string Serialize(Model obj)
        {
            var strProperties = new List<string>();
            var properties = typeof(Model).GetProperties();
            foreach (var prop in properties)
            {
                if (prop.IsDefined(typeof(CaptionAttribute)))
                {
                    var attrs = prop.GetCustomAttributes(true);
                    strProperties.Add(((CaptionAttribute) attrs[0]).Caption + ":" + prop.GetValue(obj));
                }
                else
                {
                    strProperties.Add(prop.Name + ":" + prop.GetValue(obj));
                }
            }

            return string.Join(';', strProperties);
        }

        public Model Deserialize(string str)
        {
            var strProperties = str.Split(";");
            var properties = typeof(Model).GetProperties();

            var obj = new Model();

            foreach (var strProp in strProperties)
            {
                var propName = "";
                var strParts = strProp.Split(":");
                if (properties.Any(p => p.Name == strParts[0]))
                {
                    propName = strParts[0];
                }
                else
                {
                    foreach (var prop in properties)
                    {
                        var attrs = prop.GetCustomAttributes(true);
                        if (attrs.Length == 0)
                        {
                            continue;
                        }
                        var attr = ((CaptionAttribute) attrs[0]).Caption;
                        if (attr == strParts[0])
                        {
                            propName = prop.Name;
                            break;
                        }
                    }
                }

                if (propName == "") continue;

                switch (propName)
                {
                    case "Id":
                        obj.Id = Convert.ToString(strParts[1]);
                        break;
                    case "Value":
                        obj.Value = Convert.ToString(strParts[1]);
                        break;
                }
            }

            return obj;
        }
    }
}