﻿using System;
using System.Collections.Generic;

namespace Task3
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var obj1 = new Product<int> {Id = 1, Name = "Fruit"};
            var obj2 = new Product<int> {Id = 2, Name = "Vegetable"};
            var obj3 = new Product<int> {Id = 3, Name = "Lemonade"};

            var storage = new Storage();
            var logger = new Logger();
            
            var manager = new Manager(storage, logger);

            manager.Add(obj1);
            manager.Add(obj2);
            manager.Add(obj3);

            manager.Delete(obj2);

            manager.Update(obj2, "Meat");

            manager.Get(1);
        }
    }

    public interface IStorage<T>
    {
        void Add(T obj);
        void Delete(T obj);
        T Get(int index);
        void Update(T obj, string newValue);
    }

    public interface ILogger
    {
        void Log(string text);
    }

    public class Product<TId>
    {
        public TId Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id:{Id}; Name:{Name}";
        }
    }

    public class Storage : IStorage<Product<int>>
    {
        public readonly List<Product<int>> ProductList = new List<Product<int>>();

        public void Add(Product<int> obj)
        {
            ProductList.Add(obj);
        }

        public void Delete(Product<int> obj)
        {
            ProductList.Remove(obj);
        }

        public Product<int> Get(int index)
        {
            return index > ProductList.Count ? new Product<int>() : ProductList[index];
        }

        public void Update(Product<int> obj, string newValue)
        {
            obj.Name = newValue;
        }
    }

    public class Logger : ILogger
    {
        public void Log(string text)
        {
            Console.WriteLine(text);
        }
    }

    public class Manager
    {
        private readonly IStorage<Product<int>> _storage;
        private readonly ILogger _logger;

        public Manager(IStorage<Product<int>> storage, ILogger logger)
        {
            _storage = storage;
            _logger = logger;
        }
        
        public void Add(Product<int> obj)
        {
            _storage.Add(obj);
            _logger.Log($"Добавлен новый элемент: {obj.ToString()}");
        }

        public void Delete(Product<int> obj)
        {
            _storage.Delete(obj);
            _logger.Log($"Удален элемент: {obj.ToString()}");
        }

        public Product<int> Get(int index)
        {
            var obj = _storage.Get(index);
            _logger.Log($"Получен элемент: {obj.ToString()}");

            return obj;
        }

        public void Update(Product<int> obj, string newValue)
        {
            _storage.Update(obj, newValue);
            _logger.Log($"Обновлен элемент: {obj.ToString()}");
        }
    }
}