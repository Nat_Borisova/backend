﻿using System;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            var isbnGenerator = new IsbnGenerator();
            var code = isbnGenerator.Generate();

            var isbnChecker = new IsbnChecker();
            Console.WriteLine(code);
            Console.WriteLine(isbnChecker.IsIsbn(code) ? "Код является кодом ISBN" : "Код не является кодом ISBN");
        }
    }
    
    public class IsbnChecker
    {
        private const int IsbnLength = 22;

        public bool IsIsbn(string code)
        {
            var pattern = new Regex("ISBN 978-([0-9]|[-]){13}");

            if (!pattern.IsMatch(code) && code.Length < IsbnLength)
                return false;

            var codeNumbers = Regex.Replace(code.Substring(9), "[-]", string.Empty);
            var checkCoeff = 10;
            var sum = 0;

            foreach (var codeNumber in codeNumbers)
            {
                if (codeNumber == 'X')
                {
                    sum += 10 * checkCoeff--;
                }
                else if (int.TryParse(codeNumber.ToString(), out var number))
                {
                    sum += number * checkCoeff--;
                }
            }

            if (sum % 11 == 0 && checkCoeff == 0)
            {
                return true;
            }

            return false;
        }
    }
    
    public class IsbnGenerator
    {
        public string Generate()
        {
            var code = "ISBN 978-";

            var random = new Random();
            var sum = 0;

            for (var i = 0; i < 9; i++)
            {
                var number = random.Next(10);
                code += number;
                sum += number * (10 - i);
                if (i == 0 || i == 5 || i == 8)
                {
                    code += "-";
                }
            }

            switch (sum % 11)
            {
                case 0:
                    code += "0";
                    break;
                case 1:
                    code += "X";
                    break;
                default:
                    code += 11 - sum % 11;
                    break;
            }

            return code;
        }
    }
}

