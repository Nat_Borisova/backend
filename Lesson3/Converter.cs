using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JSONConverter
{
    public class MathObjectConverter : Converter<MathObject>
    {
        protected override MathObject Create(Type objectType, JToken jToken)
        {
            if (FieldExists("coords", jToken))
            {
                return new Point();
            }
            else if (FieldExists("points", jToken))
            {
                return new Area();
            }
            else if (FieldExists("from", jToken) && FieldExists("to", jToken))
            {
                return new Line();
            }
            else
            {
                return new MathObject();
            }
        }

        private static bool FieldExists(string fieldName, JToken jToken)
        {
            return jToken[fieldName] != null;
        }

    }

    public abstract class Converter<T> : JsonConverter
    {
        public override bool CanWrite => false;

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
        
        protected abstract T Create(Type objectType, JToken jToken);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.StartArray)
            {
                var jArray = JArray.Load(reader);

                if (jArray.Count == 0)
                {
                    return null;
                }
                
                var target = Create(objectType, jArray[0]);

                serializer.Populate(jArray[0].CreateReader(), target);

                return target;
            }

            else
            {
                var jObject = JObject.Load(reader);

                var target = Create(objectType, jObject);

                serializer.Populate(jObject.CreateReader(), target);

                return target;
            }
        }
    }

}