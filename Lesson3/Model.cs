using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace JSONConverter
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class Coordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    [JsonConverter(typeof(MathObjectConverter))]
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class MathObject
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
    
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class Point : MathObject
    {
        public Coordinate Coords { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class Area : MathObject
    {
        public Area()
        {
            Points = new Point();
        }
        
        public Point Points { get; set; }
    }
    
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class Line : MathObject
    {
        public Point From { get; set; }
        public Point To { get; set; }
    }

}