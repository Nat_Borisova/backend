﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JSONConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = @"[{'id': 1,'type': 'point','coords': {'latitude': 56.24,'longitude': 43.997}},
                          {'id': 8,'type': 'area','points': [{'id': 3,'type': 'point',
                            'coords': {'latitude': 56.75,'longitude': 43.098}}]},
                          {'id': 6,'type': 'line','from': {'id': 2,'type': 'point',
                            'coords': {'latitude': 56.328,'longitude': 43.75}},'to': {'id': 4,'type': 'point',
                                'coords': {'latitude': 56.418,'longitude': 43.9}}}]";

            var objects = JsonConvert.DeserializeObject<List<MathObject>>(json);

            var newJSON = JsonConvert.SerializeObject(objects);
        }
    }
}